const logger = require('./logger');

let counter = 0;
logger.info('[COUNTER] read 0');

const increaseCounter = () => {
counter++;
logger.info('[COUNTER] increase 1');
return counter;
};

const readCounter = () => {
return counter;
;}

const resetCounter = () => {
counter = 0;
logger.info('[COUNTER] zeroed 0');
};

module.exports = {
    increaseCounter,
    readCounter,
    resetCounter,
};


