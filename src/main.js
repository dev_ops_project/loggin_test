const express = require('express');
const app = express();
const PORT = 3000;
const router = require('./routes');
const logger = require('./logger');

app.use('/', router);

app.use('/counter-increase',router);

app.use('/counter-read',router)

app.use('/counter-reset',router);

logger.info('[MAIN] Starting');

const server = app.listen(PORT, () => {
    logger.info('Server is listening on port ' + PORT);
    console.log(`Listening on port ${PORT}`);
  });
  
  // Log stopping event when the server is closed
  process.on('SIGINT', () => {
    logger.info('[MAIN] Stopping');
    // Ensure the server is closed before exiting the process
    server.close(() => {
      process.exit(0);
    });
  });