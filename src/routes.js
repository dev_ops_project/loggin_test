const express = require('express');
const router = express.Router();
const counterController = require('./counter');
logger = require('./logger');


router.get('/', (req, res) => {
    logger.info(`ENDPOINT ${req.method} ${req.originalUrl}`);
    res.send("hello world!");
});

router.get('/counter-increase', (req, res) => {
    const newCounter = counterController.increaseCounter();
    logger.info(`ENDPOINT ${req.method} ${req.originalUrl}`);
    res.send(`counter: ${newCounter}`);
});

router.get('/counter-read', (req, res) => {
    const currentCounter = counterController.readCounter();
    logger.info(`ENDPOINT ${req.method} ${req.originalUrl}`);
    res.send(`counter: ${currentCounter}`);
});

router.get('/counter-reset', (req, res) => {
    counterController.resetCounter();
    logger.info(`ENDPOINT ${req.method} ${req.originalUrl}`);
    res.send('counter reset to zero');

});

module.exports = router;

